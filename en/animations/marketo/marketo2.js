
function Marketo2(resources)
{
	Marketo2.resources = resources;
}
Marketo2.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 400, Phaser.CANVAS, 'Marketo2', { preload: this.preload, create: this.create, update: this.update, render:
		this.render,parent:this });
	},

	preload: function()
	{
        this.game.stage.backgroundColor = '#ffffff';
        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', Marketo2.resources.bckg);
    	this.game.load.image('layer3', Marketo2.resources.layer3);
		this.game.load.image('layer2', Marketo2.resources.layer2);
		this.game.load.image('layer1', Marketo2.resources.layer1);

	},

	create: function(evt)
	{
    this.parent.game.stage.backgroundColor = '#ffffff';
    //Call the animation build function
    this.parent.buildAnimation();

	},

	cycleData: function(evt){

	},

	up: function(evt)
			{
			//	debugger
		    //console.log('button up', arguments);
	}
		,
	over: function(evt)
			{
		    //console.log('button over');
	}
		,
	out: function(evt)
			{
		    //console.log('button out');
	}
	,

	buildAnimation: function()
	{


    //Background
    this.bckg = this.game.add.sprite(0, 0, 'bckg');
   //this.bckg.anchor.set(0.5);

    //STYLE


//var topStyle = {font:"bold 18px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 200, align: "center"};
//var downrightStyle = {font:"bold 18px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 200, align: "right"};
//var downleftStyle = {font:"bold 18px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 200, align: "left"};
 //   var percentStyle = {font:"bold 22px freight-sans-pro", fill: "#4569af", wordWrap: true, wordWrapWidth: 300, align: "center"};
  //  var footerStyle = {font:"bold 16px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 180, align: "center"};


var percentStyle = Marketo2.resources.percentStyle;
//{font:"bold 30px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 500, align: "center"};
var footerStyle = Marketo2.resources.footerStyle;
//{font:"bold 16px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth: 500, align: "center"};


this.layer1 = this.game.add.sprite(-450, 30, 'layer1');
this.layer2 = this.game.add.sprite(-450, 30, 'layer2');
this.layer3 = this.game.add.sprite(-450, 30, 'layer3');

this.pfield_1 = this.game.add.text(0, 0, Marketo2.resources.ico1text, percentStyle);
this.pfield_2 = this.game.add.text(0, 0, Marketo2.resources.ico2text, percentStyle);
this.pfield_3 = this.game.add.text(0, 0, Marketo2.resources.ico3text, percentStyle);

this.pfieldA_1 = this.game.add.text(0, 0, Marketo2.resources.ico1textA, footerStyle);
this.pfieldA_2 = this.game.add.text(0, 0, Marketo2.resources.ico2textA, footerStyle);
this.pfieldA_3 = this.game.add.text(0, 0, Marketo2.resources.ico3textA, footerStyle);



 //this.layer1.addChild(this.pfield_1);
  this.pfield_1.x =  -500; //this.layer1.width / 2;
  this.pfield_1.y =  180;
  this.pfield_1.anchor.set(0.5);

 //this.layer2.addChild(this.pfield_2);
 this.pfield_2.x =  -500; //this.layer2.width / 2;
 this.pfield_2.y =  180;
  this.pfield_2.anchor.set(0.5);

  //this.layer3.addChild(this.pfield_3);
 this.pfield_3.x =  -500; //this.layer3.width / 2;
 this.pfield_3.y =  180;
 this.pfield_3.anchor.set(0.5);/* */


//this.layer1.addChild(this.pfieldA_1);
this.pfieldA_1.x =  -500;  //this.layer1.width / 2;
this.pfieldA_1.y =  250;
this.pfieldA_1.anchor.set(0.5);

this.pfieldA_2.x =  -500;  //this.layer1.width / 2;
this.pfieldA_2.y =  250;
this.pfieldA_2.anchor.set(0.5);


this.pfieldA_3.x =  -500;  //this.layer1.width / 2;
this.pfieldA_3.y =  250;
this.pfieldA_3.anchor.set(0.5);
/*
///this.layer2.addChild(this.pfieldA_2);
 this.pfieldA_2.x =  this.layer2.width / 2;
 this.pfieldA_2.y =  200;
this.pfieldA_2.anchor.set(0.5);


//this.layer3.addChild(this.pfieldA_3);
 this.pfieldA_3.x = this.layer3.width / 2;
 this.pfieldA_3.y =  200;
this.pfieldA_3.anchor.set(0.5);*/


this.tweenlayer1 = this.game.add.tween(this.layer1).to( { x: 100}, 1000, Phaser.Easing.Linear.Out, true, 10);

this.textTween1 = this.game.add.tween(this.pfield_1).to( { x: 160}, 1000, Phaser.Easing.Linear.Out, true, 1000);

this.textTween1a = this.game.add.tween(this.pfieldA_1).to( { x: 160}, 1000, Phaser.Easing.Linear.Out, true, 1000);




this.tweenlayer2 = this.game.add.tween(this.layer2).to( { x: 350}, 1000, Phaser.Easing.Linear.Out, true, 2000);

this.textTween2 = this.game.add.tween(this.pfield_2).to( { x: 410}, 1000, Phaser.Easing.Linear.Out, true, 3000);

this.textTween2a = this.game.add.tween(this.pfieldA_2).to( { x: 410}, 1000, Phaser.Easing.Linear.Out, true, 3000);


this.tweenlayer3 = this.game.add.tween(this.layer3).to( { x: 600}, 1000, Phaser.Easing.Linear.Out, true, 4000);

this.textTween3 = this.game.add.tween(this.pfield_3).to( { x: 660}, 1000, Phaser.Easing.Linear.Out, true, 5000);

this.textTween3a = this.game.add.tween(this.pfieldA_3).to( { x: 660}, 1000, Phaser.Easing.Linear.Out, true, 5000);

	//this.tweenlayer2 = this.game.add.tween(this.layer2).to( { x: 350 }, 1000, Phaser.Easing.Linear.Out, true, 1500);

	//this.tweenlayer3 = this.game.add.tween(this.layer3).to( { x: 600 }, 1000, Phaser.Easing.Linear.Out, true, 2500);

	/*
	this.tweenlayer4 = this.game.add.tween(this.layer1).to( { x: 100}, 1000, Phaser.Easing.Linear.Out, true, 500);

	this.tweenlayer5 = this.game.add.tween(this.layer2).to( { x: 350 }, 1000, Phaser.Easing.Linear.Out, true, 1500);

	this.tweenlayer6 = this.game.add.tween(this.layer3).to( { x: 600 }, 1000, Phaser.Easing.Linear.Out, true, 2500);
	*/


	},


	actionOnClick: function(evt){

	},

	animate: function()
	{

	},

/////////////////////////////////////////////////

	update: function()
	{

	},

	render: function()
	{

	}

}
